# General Web Resources

## Code
- https://www.patterns.dev/

## Collections
- https://free-for.dev/
- https://allthefreestock.com/

## Images and Designs

- https://www.pixeltrue.com/free-illustrations
