# Useful Web Resources 

## Resources
1. [General](./general/README.md)
2. [Project Ideas](./projects/README.md)
3. [React](./react/README.md)
3. [Angular](./angular/README.md)


## Background
This repository is a collection of useful resources for web developers. I have no affiliation with the linked software/websites in the repository.

## Contribution
PRs accepted

## Author
Dewald Els
