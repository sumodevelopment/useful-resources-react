# Useful Resources - React 

Useful list of Resources for React

## React useCallback
https://dmitripavlutin.com/dont-overuse-react-usecallback/

## React.memo
https://dmitripavlutin.com/use-react-memo-wisely/

## Sample Project Structure
https://github.com/alan2207/bulletproof-react
